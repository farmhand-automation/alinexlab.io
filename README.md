# Alinex Development Guide

This is the source of my personal development book, but better read the book on it's own site:
[alinex.gitlab.io](https://alinex.gitlab.io).

For everyone which want to fix errors or help writing the book, send me pull requests with description of your changes.

_Alexander Schilling_
