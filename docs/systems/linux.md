# Linux

This is a small and incomplete collection of some Problems I had to fix, to keep them if I may need them again.

## Screen

To work with long running scripts `nohup` may be used. But to have better and further control `screen` will help. It also allows to share the terminal session.

Start a new session:

```bash
screen      # new default session
screen -S my_name # named session
```

Within the screen the following commands will help:

-   ++ctrl+a++ folowed by ++ctrl+d++ will detach from screen and you may close the terminal

And to jump into a running screen use:

```bash
screen -r   # reattach to default session
screen -ls  # show list of screens
screen -r <num> # reattach to numbered entry
```

## Touch click events not working after suspend

Create a script which is called on resume event under `/etc/pm/sleep.d/0000trackpad`:

```bash
#!/bin/sh
case "$1" in
    resume)
        DISPLAY=:0.0 su <USER> -c '/usr/bin/synclient TouchpadOff=0' ;;
esac
```

## Tunnel Git through socks proxy

```bash
# open tunnel
ssh -D 1337 -q -C -N alinex@peacock.uberspace.de # Ctrl-C to stop
# use tunnel for git
git config --global http.proxy 'socks5://127.0.0.1:1337'
# remove tunnel from git
git config --global --unset http.proxy
```

{!docs/assets/abbreviations.md!}
