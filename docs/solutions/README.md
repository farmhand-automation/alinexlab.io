title: Overview

# Solutions

What is presented here as solutions are concepts, ideas, modules and also complete working products. See it as the good things you can get out of this whole book.

Most of the book was written while I worked on this solutions so they are the successor of this book. This site contains description about basic concepts but will only contain short overviews about each product or module with how it works and what's the purpose of it. But to really decide if it can help you solve your problems and needs you have to follow the links to further documentation to check if you may use it.

![solutions overview](solutions.svg){: .center}

As you see the modules are very well connected and integrated to build a whole system. Also shown are some of the essential third party modules. There are a lot more which could not be shown here.

The bash-lib has a special position here because it is not used directly in production use but for administration and operation tasks for this and a lot of other systems.

## Quick links

To all of the actively maintained alinex modules.

| Alinex Module | GitLab | Manual |
| ------------- | ------ | ------ |
| Async Helper  | [Code](https://gitlab.com/alinex/node-async) | [Documentation](https://alinex.gitlab.io/node-async) |
| Bash-Lib      | [Code](https://gitlab.com/alinex/node-bash) | [Documentation](https://alinex.gitlab.io/node-bash) |
| Checkup       | [Code](https://gitlab.com/alinex/node-checkup) | [Documentation](https://alinex.gitlab.io/node-checkup) |
| Core          | [Code](https://gitlab.com/alinex/node-core) | [Documentation](https://alinex.gitlab.io/node-core) |
| Data Utils    | [Code](https://gitlab.com/alinex/node-data) | [Documentation](https://alinex.gitlab.io/node-data) |
| Data Store    | [Code](https://gitlab.com/alinex/node-datastore) | [Documentation](https://alinex.gitlab.io/node-datastore) |
| GUI Client    | [Code](https://gitlab.com/alinex/node-gui) | [Documentation](https://alinex.gitlab.io/node-gui) |
| Server        | [Code](https://gitlab.com/alinex/node-server) | [Documentation](https://alinex.gitlab.io/node-server) |
| Validator     | [Code](https://gitlab.com/alinex/node-validator) | [Documentation](https://alinex.gitlab.io/node-validator) |

## Chapters

**[DevOps](devops.md)**
: This will contain solutions from the operations area which can be used to make the tasks there more automatic.

**[Applications](applications.md)**
: Real applications, which can be installed, configured and used by anybody.

**[Modules](modules.md)**
: This contains base modules which help you to start developing your project. Have a look at them and check what you need and what not. Include them as needed.

**[Playground](playground.md)**
: My example applications are working but not fully functional. They are used as a base to develop them further for your needs. You can fork them and continue the development with adding your own modules...

**[Quality Standards](quality.md)**
: These are seen as goals, more like a vision, meaning not every goal may be reached or completely reached. But you should try to fulfill these as much as possible.

Smaller language specific concepts can also be found under: [Rust](../solutions).

{!docs/assets/abbreviations.md!}
