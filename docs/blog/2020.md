title: Blog 2020

# Developer Blog

## November 2020

**Migration**: That's often no easy task and can bring a lot of trouble. I wrote some ground rules for the different levels of [migration in software projects](../concepts/migration.md). Hope that helps others too. I did not go into special tools because that depends on the type of code. Maybe I will later design an optimal way to be used in my alinex projects which currently miss such support.

**Alinex Server reached V1.0**: With the full inclusion of checkup module, IP based login and some bug fixes the server now reached the first major version. It should be ready now to use it in production. But as always some bug fixes may follow in the near future.
It is used in production as interface to internal databases and should be used for monitoring, soon.

## October 2020

**REST Calls**: While working on the server I had to often make REST Calls so I found out about the [VS Code Plugin](../env/vscode.md#rest-client) which for me is better than Postman or other tools. It is simple but powerful making all REST Calls possible with the use of environments and variables. See the before linked VS Code description in which I added how I use it with an example.

**Statistics**: As I added statistics to each module I also calculated the overall numbers for the alinex universe. Currently there are 8 active modules with 24111 lines of code and 632 pages of documentation. This shows that there is a lot of work in them.

**Documentation Theme**: I changed my documentation theme again and updated it from module to module in time. Some doc fixes are also made and statistic, support information was added.
The new theme has an abstract dark design with boxes which should make the overall look more modern and a bit futuristic.

**Gitlab CI/CD**: I had to enhance my knowledge about the possibilities of GitLab, better structure my pipeline and jobs to make it more granular. Find some of it in my enhanced description about [GitLab](../env/gitlab.md).

**Checkup for Enterprise**: The checkup system is growing further with more and more features. But the wish list is growing, too 😟 New features implemented are SubTests for Fixes.
The great ideas making the system enterprise ready will be: multi-test, automatic group, dependencies, base test, HTML and email output and more and more tests are added.
Also it's integration into [Alinex Server](https://alinex.gitlab.io/node-server) is worked on.
What really needs time are the fixes, which will have to grow while using it. New ideas are coming further in, too.

## September 2020

**Checkup Structure**: To make it more universal I had to go some steps back and reorganize the calling structure. Therefore a better class based structure with event management was setup. This makes it possible to show progress information and customize the output more. While this changes were integrated a lot of smaller optimizations for cleaner code and simpler calling structure were added. See the [Examples](https://alinex.gitlab.io/node-checkup/cli/#examples) showing the checkup system in action called by CLI.

**Checkup**: [Alinex Checkup](https://alinex.gitlab.io/node-checkup) now works fully with test/fix for three linux tests. Full internationalization is also integrated. It can check CPU, memory, load, services on local or remote linux machines. In the next steps more tests will be added and this module should be integrated into the [Alinex Server](https://alinex.gitlab.io/node-server) to run the system checkup and maybe later use the server as frontend to run tests on any system.

**Validator Internationalization**: As it works great in the new checkup system I am now in the step of switching in the Validator, too. This will need an enhancement in the API and replacing all the calls to text elements. This all should be achieved within the next days and will be released in Version 3.7 soon.<br\>
Maybe some more minor changes will als be added, too.

## August 2020

**Internationalization**: Till now I used y18n, a simple internationalization module but was missing support on packages and mainly problematic to get it working on modules. As I see four ways of internationalization: CLI, server, client and module. The CLI will set the language fixed for the whole process. For a web server the language has to change for each request and a module which is added to such webserver should be able to pick the language from the request for it's calls. The client is mainly based to what is best integrated with the framework there.\
Therefore I implemented the CLI and module implementation using [i18next](https://www.i18next.com) for the new checkup module and will integrate it further in other modules like validator and server.

**Checkup**: This is the new test system which just got the first general working state. The [Alinex Checkup](https://alinex.gitlab.io/node-checkup) is working using API or CLI and has the first two tests including a manual fix to be used. But this is only the start, some more specific functions and a lot more tests and fixes are needed to make it usable. It should later be integrated into the server, too.

**Test System**: A new test framework is being planned. It should allow also difficult and complex checks together with repair methods in a service oriented framework. It should not replace any monitoring but may be called to assist monitoring and for manual checkups. It is planned to integrate it into the Alinex Server. This will also replace the [mocha based Validator checks](https://alinex.gitlab.io/node-validator/usage/mocha/).

**Server Stability**: Because I got some pressure to use the [server](https://alinex.gitlab.io/node-server) and [GUI](https://alinex.gitlab.io/node-gui) already now productively I had to change my plans. I did the hardening first, so now I integrated validation, further optimized logging and finished support for an `action.log`. Also the use as framework is improved more, because this is the way it is used in production. The new version may be released within the next month.

## July 2020

**GUI**: The first dialog, user editing, is now working through store with feathers and the REST server using websockets. That's a big milestone for me. Also the first real project (parent project) is now basically working and the development cycle got a bit faster and cleaner.

**Multi Format REST Response**: Based on the DataStore the ALinex Server got a REST response system in which the format can be specified using accept header with a lot of possibilities from json, csv till excel files.

**Upgrade GUI**: Upgraded to the next Quasar and Quasar app version.

## June 2020

**Alinex Server Productive**: It is used at my work as backoffice system for some simple tasks in the moment. This shows that the extension of the base framework for individual use basically works and shows me the problems and optimizations. But the GUI is further under heavy development and may be usable till the end of this month. All in all this are early stages and I don't suggest to use it productively at the moment.

## May 2020

**Modular Server and GUI**: Making the REST Server modular, to be extended by specific business logic and extension modules.
This should allow to develop a core system and use these in specialized projects by adding to it or overwriting some parts.

**Mkdocs Material 5**: Transition for all modules to the next major version of mkdocs material.

## March 2020

**TS GUI Client**: I started to try to build a client using quasar with TypeScript. This is a first start of an universal frontend for [Alinex Server](https://alinex.gitlab.io/node-server). But this was not running smoothly, I had often trouble because of incomplete definitions and also had to translate each piece of code while packing together. So I decided to go back and use vanilla JS.

**Rest Server Basics**: After the [server](https://alinex.gitlab.io/node-server) is now running using TypeScript this month is a time for consolidation. The main part is to better understand the control flow and make it visual using logging.

**Static books**: Beside the HTML a PDF was available since some time but now I had to update it's creation and added also an ePub Version. As always the base HTML Site is the best.

## February 2020

**Going to the frontend direction**: This month I started to go my way further to the frontend. The earlier developed portal and portal client which was only a initial try should now be rebuild. My plan is to first complete the [server](https://alinex.gitlab.io/node-server) with authentication and access right management this month. Then to build a multi device application on top in march...

**Validator simplification**: Within the code maintenance phase I decided to make the definition of deep structures in validator simpler. To make the definition of complex structures using multiple objects and arrays within each other a new `item`definition has been implemented which replaces the previous `keys` or `items` setting. [validator](https://alinex.gitlab.io/node-validator)

## January 2020

**Code maintenance**: Good code has to be kept up to date. That means as my first thing this year will be to go over all actively used modules and update them to current standards. In this step I will try to add some smaller additions to give each also a little push forward.

{!docs/assets/abbreviations.md!}
