# REST Services

filter
search
sort +name,-age
limit
paging
fields
format json/xml
include

MongoDB Operators https://docs.mongodb.com/manual/reference/operator/query/

- https://www.vinaysahni.com/best-practices-for-a-pragmatic-restful-api
- https://blog.mwaysolutions.com/2014/06/05/10-best-practices-for-better-restful-api/
- https://www.moesif.com/blog/technical/api-design/REST-API-Design-Filtering-Sorting-and-Pagination/
