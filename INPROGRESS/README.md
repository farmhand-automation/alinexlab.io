# Technologies

Here are some basic technologies which may be used in development:

Sourcecode repositories:

-   [Git](git.md)
-   [GitLab](gitlab.md)
-   [GitHub](github.md)

Using JavaScript on the server and as development system:

-   [NodeJS](nodejs.md)
-   [NPM](npm.md) module repository
